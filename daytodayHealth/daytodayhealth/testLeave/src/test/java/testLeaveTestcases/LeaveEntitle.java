package testLeaveTestcases;

import java.io.FileInputStream;
import java.util.Properties;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import leavePage.Leave;

public class LeaveEntitle extends BaseCode {


	Leave health;
	Properties prop;
	FileInputStream fis; 
	String nameofEmployee;
	String setStartDate;
	String setEndDate;

	@BeforeMethod
	public void setup() throws Exception {
		health = PageFactory.initElements(BaseCode.driver, Leave.class);
	}

	//path of config.properties class to fetch script data
	@BeforeTest
	public void config() throws Exception{

		prop = new Properties();
		FileInputStream fis = new FileInputStream("config.properties");
		prop.load(fis); 
	}

	@Test(priority=1)
	public void login() {
		health.enterUsername(prop.getProperty("username"));
		health.enterPassword(prop.getProperty("password"));
		health.clicklogin();
	}

	@Test(priority=2)
	public void addEntitleTest() throws InterruptedException {
		String name = 	health.getName();
		String[] getEmpName = name.split(" ");
		nameofEmployee = getEmpName[1];
		health.gotoLeave();
		health.goToentitle();
		health.addentitlement();
		Thread.sleep(2000);
		health.employeesearch();
		health.employeeName(nameofEmployee);
		health.employeeSelect();
		health.incEntitlement(prop.getProperty("count"));
		health.save();
		health.confirmEntitle();
	}

	@Test(priority=3)
	public void addLeave() throws InterruptedException {
		Thread.sleep(2000);
		health.assignLeaveClick();
		health.leaveEmpSelect();
		health.enterLeaveEmp(nameofEmployee);
		health.employeeSelect();
		health.leaveTypeSelect(prop.getProperty("leaveType"));
		health.startDateClick();
		health.selectDate(prop.getProperty("startDate"));
		health.endDateClick();
		health.selectDate(prop.getProperty("endDate"));
		Thread.sleep(1000);
		health.assignLeave();
	}

	@Test(priority=4)
	public void leaveCancel() throws InterruptedException {
		SoftAssert Assert = new SoftAssert();
		Thread.sleep(2000);
		health.clickMyLeave();
		health.uncheck();
		health.checkScheduled();
		health.searchbutton();
		String name = health.getempName();
		Assert.assertTrue(name.contains(nameofEmployee));
		//		String getBalance = health.leaveBalance();
		//		String noOfdays = health.daysofleave();
		//		int bal = Integer.parseInt(getBalance);
		//		int day = Integer.parseInt(noOfdays);
		health.cancelLeave();
		health.save();
	}
	
}
