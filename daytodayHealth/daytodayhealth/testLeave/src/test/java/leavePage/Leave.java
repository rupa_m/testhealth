package leavePage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import testLeaveTestcases.BaseCode;

public class Leave{

	public static WebDriver driver;
	Actions action = new Actions(BaseCode.driver);
	WebDriverWait wait = new WebDriverWait(BaseCode.driver,20);

	public Leave(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(id="txtUsername")
	WebElement username;

	@FindBy(id="txtPassword")
	WebElement password;

	@FindBy(id="btnLogin")
	WebElement login;

	@FindBy(id="menu_leave_viewLeaveModule")
	WebElement leave;

	@FindBy(id="menu_leave_Entitlements")
	WebElement entitlement;

	@FindBy(id="menu_leave_addLeaveEntitlement")
	WebElement addEntitle;

	@FindBy(id="entitlements_employee_empName")
	WebElement empName;

	@FindBy(xpath="//li[@class='ac_even ac_over']")
	WebElement empSelect;

	@FindBy(id="entitlements_entitlement")
	WebElement countEntitle;

	@FindBy(id="btnSave")
	WebElement saveEntitle;

	@FindBy(id="dialogUpdateEntitlementConfirmBtn")
	WebElement confirm;

	@FindBy(id="menu_leave_assignLeave")
	WebElement leaveassign;

	@FindBy(id="assignleave_txtEmployee_empName")
	WebElement leaveEmp;

	@FindBy(id="assignleave_txtLeaveType")
	WebElement leavetype;

	@FindBy(id="welcome")
	WebElement Accname;

	@FindBy(id="assignleave_txtFromDate")
	WebElement startDate;

	@FindBy(id="assignleave_txtToDate")
	WebElement endDate;

	@FindBy(id="assignBtn")
	WebElement assign;

	@FindBy(id="menu_leave_viewMyLeaveList")
	WebElement myleaveClick;

	@FindBy(id="leaveList_chkSearchFilter_checkboxgroup_allcheck")
	WebElement uncheckAll;

	@FindBy(id="leaveList_chkSearchFilter_2")
	WebElement Scheduled;

	@FindBy(id="btnSearch")
	WebElement searchbtn;

	@FindBy(xpath="(//td[@class='left'])[2]")
	WebElement getEmpLeavename;

//	@FindBy(xpath="(//td[@class='right'])[1]")
//	WebElement balance;
//		
//	@FindBy(xpath="(//td[@class='right'])[2]")
//	WebElement days;
	
	@FindBy(xpath="//select[starts-with(@id,'select_leave_action_')]")
	WebElement reject;
	
	public void enterUsername(String name) {
		username.sendKeys(name);
	}

	public void enterPassword(String pass) {
		password.sendKeys(pass);
	}

	public void clicklogin() {
		login.click();
	}

	public void gotoLeave() {
		action.moveToElement(leave).click().build().perform();
	}

	public void goToentitle() {
		action.moveToElement(entitlement).release().build().perform();
	}

	public void addentitlement() {
		action.moveToElement(addEntitle).click().build().perform();
	}

	public void employeesearch() throws InterruptedException {
		empName.click();
	}

	public void employeeName(String emplo) {
		empName.sendKeys(emplo);
	}

	public void employeeSelect() {
		action.moveToElement(empSelect).click().build().perform();
	}

	public void incEntitlement(String count) {
		countEntitle.sendKeys(count);
	}

	public void save() {
		saveEntitle.click();
	}

	public void confirmEntitle() {
		confirm.click();
	}

	public void assignLeaveClick() {
		action.moveToElement(leaveassign).click().build().perform();
	}

	public void leaveEmpSelect() {
		leaveEmp.click();
	}

	public void enterLeaveEmp(String name) {
		leaveEmp.sendKeys(name);
	}

	public void leaveTypeSelect(String leave) {
		Select select = new Select(leavetype);
		select.selectByValue(leave);
	}

	public String getName() {
		return Accname.getText();
	}

	public void startDateClick() {
		startDate.click();
	}

	public void endDateClick() {
		endDate.click();
	}

	public void selectDate(String date) {
		WebElement dateEnter = driver.findElement(By.xpath("//td[@data-month='7']//descendant::a[text()='"+date+"']"));
		dateEnter.click();
	}
	public void assignLeave() {
		action.moveToElement(assign).click().build().perform();
	}

	public void clickMyLeave() {
		action.moveToElement(myleaveClick).click().build().perform();
	}

	public void uncheck() {
		uncheckAll.click();
	}

	public void checkScheduled() {
		Scheduled.click();		
	}

	public void searchbutton() {
		searchbtn.click();
	}

	public String getempName() {
		return getEmpLeavename.getText();
	}
	
//	public String leaveBalance() {
//		return balance.getText();
//	}
//	
//	public String daysofleave() {
//		return days.getText();
//	}
	
	public void cancelLeave() {
		Select select = new Select(reject);
		select.selectByValue("96");
	}
}
